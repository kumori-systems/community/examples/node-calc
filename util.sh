#!/bin/bash

# Cluster variables
CLUSTERNAME="..."
REFERENCEDOMAIN="test.kumori.cloud"
CLUSTERCERT="cluster.core/wildcard-test-kumori-cloud"

# Service variables
INBOUNDNAME="calcnodeinb"
DEPLOYNAME="calcnodedep"
DOMAIN="calcnodedomain"
SERVICEURL="calc-node-${CLUSTERNAME}.${REFERENCEDOMAIN}"

KUMORI_SERVICE_MODEL_VERSION="1.1.6"

# Change if you want to use special binaries
KAM_CMD="kam"
KAM_CTL_CMD="kam ctl"

case $1 in

'refresh-dependencies')
  cd manifests
  # Dependencies are removed and added again just to ensure that the right versions
  # are used.
  # This is not necessary, if the versions do not change!
  ${KAM_CMD} mod dependency --delete kumori.systems/kumori
  ${KAM_CMD} mod dependency kumori.systems/kumori/@${KUMORI_SERVICE_MODEL_VERSION}
  # Just for sanitize: clean the directory containing dependencies
  rm -rf ./cue.mod
  cd ..
  ;;

'create-domain')
  ${KAM_CTL_CMD} register domain $DOMAIN --domain $SERVICEURL
  ;;

'deploy-inbound')
  ${KAM_CTL_CMD} register inbound $INBOUNDNAME \
    --domain $DOMAIN \
    --cert $CLUSTERCERT
  ;;

# To check if our module is correct (from the point of view of the CUE syntax and
# the Kumori service model), we can use "kam process" to generate the JSON
# representation
'dry-run')
  ${KAM_CMD} process deployment -t ./manifests
  ;;

'deploy-service')
  ${KAM_CMD} service deploy -d deployment -t ./manifests $DEPLOYNAME -- \
    --comment "Calc service" \
    --wait 5m
  ;;

'link')
  ${KAM_CTL_CMD} link $DEPLOYNAME:calc $INBOUNDNAME:inbound
  ;;

'describe')
  ${KAM_CMD} service describe $DEPLOYNAME
  echo
  echo
  ${KAM_CMD} service describe $INBOUNDNAME
  ;;

'describe-json')
  ${KAM_CMD} service describe $DEPLOYNAME -- -ojson
  echo
  echo
  ${KAM_CMD} service describe $INBOUNDNAME -- -ojson
  ;;

'deploy-all')
  $0 create-domain
  $0 deploy-inbound
  $0 deploy-service
  $0 link
  ;;

'update-service')
  ${KAM_CMD} service update -d deployment -t ./manifests $DEPLOYNAME -- \
    --comment "Updating calc service" \
    --wait 5m
  ;;

# Test the calculator
'test')
  curl -sS -X POST  -d '{"expr": "sin(90)*5"}' -H "Content-Type: application/json" https://${SERVICEURL}/calc  | jq .
  #curl -sS -X POST  -d '{"expr": "wrong + expression"}' -H "Content-Type: application/json" https://${SERVICEURL}/calc  | jq .
  ;;

'unlink')
  ${KAM_CTL_CMD} unlink $DEPLOYNAME:calc $INBOUNDNAME:inbound
  ;;

'undeploy-service')
  ${KAM_CTL_CMD} unregister deployment $DEPLOYNAME --wait 5m
  ;;

'undeploy-inbound')
  ${KAM_CTL_CMD} unregister deployment $INBOUNDNAME --wait 5m
  ;;

'delete-domain')
  ${KAM_CTL_CMD} unregister domain $DOMAIN
  ;;

'undeploy-all')
  $0 unlink
  $0 undeploy-service
  $0 undeploy-inbound
  $0 delete-domain
  ;;

*)
  echo "This script doesn't contain that command"
	;;

esac